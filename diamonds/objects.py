#! /usr/env/bin python
# coding: utf-8
# Author: João S. O. Bueno
# License: GPL v 3.0

from glob import glob
from itertools import product
from pygame.sprite import Sprite, Group
from pygame import image
from constants import *


def load_images(image_glob):
    return [image.load(image_name) for image_name in sorted(glob(IMAGEDIR + image_glob))]

def load_image(image_name):
    return image.load(IMAGEDIR + image_name)

Objects = {}

GameElements = Group()

class DiamondsObjectBase(Sprite):
    identifier = None
    weight = 0
    char_can_push = False
    images = []
    flat = False
    pickable = False

    falling_delay = 6
    moving_rate = 3
    hardness = 10
    strength = 5
    dying_remains = SPACE
    time_to_die = 5
    image_change_delay = 4


    def __init_subclass__(cls):
        identifier = cls.__dict__.get("identifier", None)
        if identifier:
            print ("registering", cls.__name__)
            Objects[cls.identifier] = cls
        super().__init_subclass__()

    def __init__(self, map, pos=None, *groups):
        #
        # Adding all created elements to a default group
        # gives use the information of who is alive or not
        # with the Sprite.kill method
        #
        groups = groups + (GameElements,)
        super().__init__(*groups)
        if pos:
            self.x, self.y = pos
        self.map = map
        self.falling = False
        self.falling_counter = 0
        self.image_index = 0
        self.image_change_counter = 0
        self.image = self.images[0]
        self.changed_image = True
        self.dying = False

    def update(self):
        if len(self.images) > 1:
            self.image_change_counter -= 1
        if  self.image_change_counter <= 0:
            self.image_change_counter = self.image_change_delay
            self.image_index += 1
            if self.image_index >= len(self.images):
                self.image_index = 0
            self.image = self.images[self.image_index]
            self.changed_image = True
            return
        self.changed_image = False


    def __hash__(self):
      return hash(id(self))

    def __eq__(self, other):
        # Uses the character constant for drawing text maps
        if isinstance(other, self.__class__):
            return True
        return self.__class__.identifier == other

    def move(self, delta):
        if self.can_move(delta):
            self._real_move(delta)
            return True
        return False

    def _real_move(self, movement):
        dx, dy = movement
        x, y = self.pos
        self.map[x + dx, y + dy] = self
        self.map[x, y] = Space(self.map)

    def can_move(self, movement):
        dx, dy = movement
        to_x, to_y = self.x + dx, self.y + dy
        if not self.map.valid_pos((to_x, to_y)):
            return False
        return self.map[to_x, to_y].hardness == 0

    def picked(self):
        self._remove()

    def kill(self):
        self.dying = True
        countdown = self.time_to_die * self.moving_rate
        self.map.scheduler.add(self._remove, timedelta=countdown + 1)
        self.map[self.pos] = Dying(countdown, self.map, self.pos)

    def _remove(self):
        # This kill is from the Sprite in Pygame -
        # it removes the object from all groups
        res = super(DiamondsObjectBase, self).kill()
        self.map[self.pos] = Objects[self.dying_remains](self.map, self.pos)
        return res

    def hit(self, element=None):
        pass

    pos = property(lambda s: (s.x ,s.y))

class FallingObject(DiamondsObjectBase):
    weight = 1
    def update(self, *args, **kw):
        self.fall()
        return super(FallingObject, self).update(*args, **kw)

    def fall(self):
        if self.can_move(DOWN):
            if not self.falling:
                self.falling_counter = self.falling_delay + 1
            self.falling = True
            self.falling_counter -= 1
            if not self.falling_counter:
                self.falling_counter = self.moving_rate
                self._real_move(DOWN)
            return True

        if self.falling and self.y < self.map.height - 1:
            self.map[self.x, self.y + 1].hit(self)
            # Hitting something might kill us:
            if not self.alive():
                return False
        self.falling = False

        # Moving further Down is not possible -
        # check if we can fall to the sides
        if self.y >= (self.map.height - 1) or self.map[self.x, self.y +1].flat:
            # Can't fall on the botton or standing on flat objects
            return False

        for hor_direction in (-1, 1):
            if self.can_move((hor_direction, 0)) and self.can_move((hor_direction, 1)):
                self.falling = True
                # One brief passing through the side cell -
                # We don want our elements to go through hyperspace
                self.falling_counter = 1
                self._real_move((hor_direction, 0))
                return True

        return False

class Rock(FallingObject):
    identifier = ROCK
    char_can_push = True
    images = [load_image("rock.png")]

class Diamond(FallingObject):
    identifier = DIAMOND
    pickable = True
    char_can_push = True
    images = load_images("diamond*.png")

    def picked(self):
        self.map.game.score += self.map.meta_data.get("points_per_diamond", 1)
        self.map.collected_diamonds += 1
        if (self.map.collected_diamonds >= 
                self.map.meta_data.get("required_diamonds", 100) and
                not getattr(self.map, "USE_EXIT", True)):
            raise LevelCleared
        return super(Diamond, self).picked()

class Brick(DiamondsObjectBase):
    identifier = BRICK
    weight = 0
    char_can_push = False
    images = [load_image("brick.png")]
    hardness = 20

class Wall(DiamondsObjectBase):
    identifier = WALL
    weight = 0
    char_can_push = False
    images = [load_image("wall.png")]
    hardness = 500

class Ground(DiamondsObjectBase):
    identifier = GROUND
    weight = 0
    char_can_push = False
    images = [load_image("ground.png")]
    flat = True
    hardness = 2


class Space(DiamondsObjectBase):
    identifier = SPACE
    weight = 0
    char_can_push = False
    images = [None]
    hardness = 0

class Exit(DiamondsObjectBase):
    identifier = EXIT
    weight = 0
    char_can_push = False
    images = [load_image("wall.png")]
    images_open = [load_image("wall.png"), load_image("wall_open.png")]
    hardness = 500
    pickable = False

    exit_open = False

    def update(self):
        super(Exit, self).update()
        # FIXME: use an event subscription system
        # and add a callback to open the exit
        if (not self.exit_open and
            self.map.collected_diamonds >= 
                self.map.meta_data.get("required_diamonds", 100)):
            self.exit_open = True
            self.pickable = True
            self.images = self.images_open

    def picked(self):
        raise LevelCleared


class Gypsum(DiamondsObjectBase):
    identifier = GYPSUM
    weight = 0
    char_can_push = True
    images = [load_image("gypsum.png")]
    hardness = 10
    flat = True

    def move(self, delta):
        x, y = self.pos
        if super(Gypsum, self).move(delta):
            if y > 0 and self.map[x, y - 1].weight:
                self.map[x, y - 1].move(delta)
            return True
        return False


class Dying(DiamondsObjectBase):
    identifier = DYING
    images = [load_image("ground.png")]
    char_can_push = False
    flat = True
    hardness = 23
    weight = 0

    def __init__(self, countdown, *args, **kw):
        super(Dying, self).__init__(*args, **kw)
        self.countdown = countdown
        self.image_change_counter = 0

    def update(self):
        self.countdown -= 1
        if self.countdown <= 0:
            self._remove()
            return
        return super(Dying, self).update()


class ExplodingObject(DiamondsObjectBase):
    exploding_remains = SPACE
    armed = True

    def hit(self, element=None):
        return self.kill()

    def kill(self):
        if not self.dying:
            self.dying = True
            self.strength = 25
            self.explode()
            return
        super(ExplodingObject, self).kill()

    def explode(self):
        for dx, dy in product((-1, 0, 1), (-1, 0, 1)):
            px = self.x + dx; py = self.y + dy
            if not self.map.valid_pos((px, py)):
                continue
            if self.map[px,py].hardness <= self.strength:
                if (dx, dy) !=  (0,0):
                    # FEATURE: an exploding object can turn into
                    # one thing (dying_remains) and explode
                    # things around into other things
                    self.map[px,py].dying_remains = self.exploding_remains
                self.map[px,py].kill()


class LethalObject(ExplodingObject):
    def update(self, *args, **kw):
        if abs(self.x - self.map.character.x) + abs(self.y - self.map.character.y) <= 1:
            self.strength = 25
            self.explode()
        return super(LethalObject, self).update(*args, **kw)


class WalkerObject(DiamondsObjectBase):
    directions = UP, RIGHT, DOWN, LEFT
    rev_directions = directions[::-1]


    def __init__(self, *args, **kw):
        self.direction = UP
        self.moving_counter = self.moving_rate
        self.already_turned = False
        super(WalkerObject, self).__init__(*args, **kw)

    def turn(self, ammount, directions, commit=True):
        base =  directions.index(self.direction)
        new_direction = directions[(base + ammount) % len(directions)]
        if self.can_move(new_direction):
            if commit:
                self.direction = new_direction
            return True
        return False

    def turn_right(self, ammount, commit=True):
        return self.turn(ammount, self.directions, commit)

    def turn_left(self, ammount, commit=True):
        return self.turn(ammount, self.rev_directions, commit)

    def update(self, *args, **kw):
        super(WalkerObject, self).update(*args, **kw)
        left_direction = (self.directions.index(self.direction) + 3) % 4
        if not self.can_move(self.direction):
            ammount = 1
            while ammount < 4:
                if self.turn_right(ammount):
                    break
                ammount += 1
                self.already_turned = True
            else:
                return
        if not self.already_turned:
            self.turn_left(1)
            self.already_turned = True

        self.moving_counter -= 1
        if self.moving_counter <= 0:
            self.move(self.direction)
            self.moving_counter = self.moving_rate
            self.already_turned = False

class Bomb(ExplodingObject, FallingObject):
    identifier = BOMB
    char_can_push = True
    images = [load_image("bomb.png")]
    strength = 25

    def fall(self):
        was_falling = self.falling
        result = super(Bomb, self).fall()
        if not self.falling and was_falling:
            if self.armed:
                return self.explode()
            self.armed = True
        return result


class Pillow(FallingObject):
    identifier = PILLOW
    char_can_push = True
    images = [load_image("brick.png")]
    hardness = 10
    flat = True

    def hit(self, element):
        super(Pillow, self).hit(element)
        element.armed = False


class Monster(WalkerObject, LethalObject):
    identifier = MONSTER
    images = load_images("bicho_*.png")
    moving_rate = 3


class Bat(Monster):
    identifier = BAT
    exploding_remains = DIAMOND
    images = load_images("bat_*.png")


class Character(ExplodingObject):
    identifier = CHARACTER
    weight = 0
    char_can_push = None
    images_front = images = [load_image("character_front.png")]
    images_left = load_images("character_left_*.png")
    images_right = load_images("character_right_*.png")
    flat = True

    moving_rate = 2
    strength = 5

    image_change_delay = 100
    def __init__(self, map, *args):
        super(Character, self).__init__(map, *args)
        map.character = self
        self.moving = False

    def move(self, direction):
        if self.dying:
             return False
        cls = self.__class__
        if not direction or direction == (0,0):
            self.images = cls.images_front
            return False
        dx, dy = direction
        if dx > 0:
            self.images = cls.images_right
        elif dx < 0:
            self.images = cls.images_left
        else:
            self.images = cls.images_front

        if not self.moving:
            self.moving = True
            self.moving_counter = self.moving_rate
            self.previous_direction = direction

        self.moving_counter -= 1

        to_x, to_y = self.x + dx, self.y + dy

        if self.moving_counter <= 0 and self.moving_counter % self.moving_rate == 0:
            # Update movement, even if running against a wall
            self.image_change_counter = -1
        if  self.moving_counter <= 0 and self.map.valid_pos((to_x, to_y)):
            element = self.map[to_x, to_y]
            # print element.__class__.__name__, element.pickable
            if element.hardness < self.strength or element.pickable:
                # element can be overrun and life goes on
                element.picked()
            elif not element.char_can_push:
                # there is an element that can't be pushed where we want to go.
                return False
            elif dy and element.weight:
                # can't push elements that fall on the vertical
                return False
            elif not element.can_move((dx,dy)):
                # there is a pushable element there, but it can't be pushed
                return False
            else:
                # There is a pushable element
                # where we want to move to, and we can do it!
                element.move((dx, dy))
            self.moving_counter = self.moving_rate
            self._real_move((dx, dy))
            self.map.track.append(self.pos)
            return True

    def _remove(self):
        super(Character, self)._remove()
        raise CharacterDeath


