#! /usr/env/bin python
# coding: utf-8
# Author: João S. O. Bueno
# License: GPL v 3.0

import sys
import pygame
from pygame.locals import *

import ast

from constants import *
from objects import Objects, Rock, Diamond, Brick, Space, Character

from bisect import bisect

class Scheduler(object):
    def __init__(self):
        self.ticks = 0
        self.events = []

    def update(self):
        self.ticks += 1
        while self.events and self.events[0][0] <= self.ticks:
            tick, callback, args, kw = self.events.pop(0)
            callback(*args, **kw)

    def add(self, callback, args=(), kw=None, timedelta=None, tick=None):
        if kw is None:
            kw = {}
        if timedelta and tick or not(timedelta or tick):
            raise ValueError("One of timedelta or absolute tick should be set")
        if timedelta:
            tick = self.ticks + timedelta
        index = bisect(self.events, (tick, ))
        self.events.insert(index, (tick, callback, args, kw) )


class Map(object):
    def __init__(self, text_map):
        self.width = len(text_map.split("\n",1)[0])
        self.height = text_map.count("\n")
        self.data = []
        for y, line in enumerate(text_map.split("\n")):
            map_line = []
            for x, block in enumerate(line):
                element = Objects[block](self, (x,y))
                map_line.append(element)
            self.data.append(map_line)
        self.images = {}
        self.collected_diamonds = 0
        self.track = []
        self.scheduler = Scheduler()

    def valid_pos(self, pos):
        x, y = pos
        return 0 < x < self.width and 0 < y < self.height

    def update(self):
        self.scheduler.update()
        for element in self:
            if not element.dying:
                element.update()

    def get_bs(self, screen):
        w, h = screen.get_size()
        h -= FONT_SIZE
        return w // self.width, h //self.height

    def pre_render(self, screen, element):
        image = element.image
        bl_width, bl_height = self.get_bs(screen)
        rendered_image = pygame.transform.smoothscale(image, (bl_width, bl_height))
        element_name = element.__class__.__name__.lower()
        if  element_name + "_color" in self.meta_data:
            color = self.meta_data[element_name + "_color"]
            color_block = pygame.Surface((bl_width, bl_height))
            color_block.fill(color)
            rendered_image.blit(color_block, (0,0), special_flags=BLEND_RGBA_MULT)

        self.images[id(image)] = rendered_image

    def _clear(self, screen, pos, size):
        x, y = pos
        stx, sty = size
        color = self.meta_data.get("space_color", BACKGROUND_COLOR)
        pygame.draw.rect(screen, color, (stx * x, FONT_SIZE + sty * y, stx, sty))

    def __iter__(self):
        for x in range(self.width):
            for y in range(self.height):
                yield self[x,y]

    def render(self, screen):
        stx, sty = self.get_bs(screen)
        for position in self.track:
            self._clear(screen, position, (stx, sty))
        for element in self:
            if element == SPACE:
                self._clear(screen, element.pos, (stx,sty))
            elif not element.dying:
                if element.changed_image:
                    self._clear(screen, element.pos, (stx,sty))
                if not(id(element.image) in self.images):
                    self.pre_render(screen, element)
                image = self.images[id(element.image)]
                x, y = element.pos
                screen.blit(image, (stx * x, FONT_SIZE + sty * y))
        self.track = []

    def __setitem__(self, index, item):
        self.data[index[1]][index[0]] = item
        item.x = index[0]
        item.y = index[1]

    def __getitem__(self, index):
        return self.data[index[1]][index[0]]


class Game(object):
    def __init__(self, screen):
        self.screen = screen
        self.score = 0
        self.lives = 3
        self.init_fonts()

    def start_level(self, map_name):
        self.load_map(map_name)
        while True:
            try:
                self.run()
            except LevelCleared:
                print("Score: %s\nLives: %s\nLevel: %s" % (
                    self.score, self.lives, self.map.meta_data["next_map"])
                )
                self.load_map(self.map.meta_data["next_map"])

            except CharacterDeath:
                self.lives -= 1
                print("remaining lives", self.lives)
                # FIXME: We need the delay bellow while into having an animation for char death and map reloading
                # if the CharDeath is related to an input condition (like pressing the abort cave button)
                # We need time to let the key go!
                pygame.time.delay(10 * FRAME_DELAY)
                if self.lives == 0:
                    raise GameOver
                self.load_map(self.current_map_name)
                clear_screen(self.screen)

    def run(self):
        counter = 0
        while True:
            self.mainloop_body()
            if not counter % 30:
                self.update_display()

    def update_display(self):
        m1 = u"DIAMONDS %d/%d" % (self.map.collected_diamonds,
            self.map.meta_data["required_diamonds"])
        m2 = u"SCORE %d" % self.score
        #m3 = u"MAP %s" % self.current_map_name.split(".",1)[0]
        self.display (m1, m2)

    def mainloop_body(self):
        pygame.event.pump()
        direction = get_input()
        self.map.character.move(direction)
        self.map.update()
        self.map.render(self.screen)
        pygame.display.flip()
        pygame.time.delay(FRAME_DELAY)

    def load_map(self, name):
        """
        Loads a map file-
        the map file should be a plain text file, characters
        beyound ASCII do not have special meaning, so the
        encoding should be whaterver ast expects it to be (likely utf-8 in Py 3)

        Each line in the  beggining of the file is a pair of key, value separated
        by the "=" signal.
        The last line from this section should contain the key "data"
        The following lines are taken to be the drawing of the map,
        with each char meaning a different object.
        """
        data = open(MAPDIR + name, "rt").readlines()
        meta_data = {}
        for linenum, line in enumerate(data):
            if not line.strip() or line.strip().startswith("#"):
                continue
            key, value = line.split("=",1)
            key = key.strip()
            if key == "data":
                break
            value = ast.literal_eval(value.strip())
            meta_data[key] = value
        map_data = "".join(data[linenum+1:])
        map = Map(map_data)
        map.meta_data  = meta_data
        self.map = map
        self.current_map_name = name
        map.game = self
        clear_screen(self.screen, self.map.meta_data.get("space_color", BACKGROUND_COLOR))

    def init_fonts(self):
        self.font = pygame.font.Font(FONT_FILE, FONT_SIZE)
        self.small_font = pygame.font.Font(FONT_FILE, FONT_SIZE // 2)

    def display(self, *messages):
        color = self.map.meta_data.get("space_color", BACKGROUND_COLOR)
        width = self.screen.get_size()[0]
        pygame.draw.rect(self.screen, color,
            (0,0, width, FONT_SIZE))
        texts = []
        for message in messages:
            texts.append(self.font.render(message, True, FONT_COLOR))
        for x, text in zip(range(0, width, width // len(messages)), texts):
            self.screen.blit(text, (x, -FONT_SIZE //5 ))


def get_input():
    keys = pygame.key.get_pressed()
    if keys[K_LEFT]:
        direction = LEFT
    elif keys[K_RIGHT]:
        direction = RIGHT
    elif keys[K_UP]:
        direction = UP
    elif keys[K_DOWN]:
        direction = DOWN
    elif keys[K_q]:
        raise CharacterDeath
    elif keys[K_ESCAPE]:
        raise GameOver
    else:
        direction = None
    return direction


def main(screen, map_name):
    game = Game(screen)
    try:
        game.start_level(map_name + ".map")
    except GameOver:
        print(game.score)

def init():
    pygame.init()
    screen = pygame.display.set_mode(SIZE)
    clear_screen(screen)
    return screen

def clear_screen(screen, color=BACKGROUND_COLOR):
    pygame.draw.rect(screen, color, (0,0) + screen.get_size())

def quit():
    pygame.quit()

if __name__ == "__main__":
    try:
        screen = init()
        main(screen, sys.argv[1] if len(sys.argv) >= 2 else "test")
    finally:
        quit()
