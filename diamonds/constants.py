#! /usr/env/bin python
# coding: utf-8
# Author: João S. O. Bueno
# License: GPL v 3.0

# Configuration constants
SIZE = 800,600
FRAME_DELAY = 30
FONT_SIZE = 40

IMAGEDIR = "images/"
MAPDIR = "maps/"
FONT_FILE = "resources/ArchivoBlack-Regular.ttf"


ROCK = "R"
DIAMOND = "D"
SPACE = " "
BRICK = "@"
GROUND = "G"
EXIT = "E"
WALL = "#"
CHARACTER = "*"
GYPSUM = "Y"
BOMB = "B"
MONSTER = "M"
BAT = "Z"
PILLOW = "P"

DYING = "kabum"

#constants used for direction movement
UP, DOWN, LEFT, RIGHT = (0,-1), (0,1), (-1,0), (1, 0)

BACKGROUND_COLOR = 64, 128, 192
FONT_COLOR = 255, 255, 255

class GameOver(Exception):
    pass

class CharacterDeath(Exception):
    pass

class LevelCleared(Exception):
    pass
